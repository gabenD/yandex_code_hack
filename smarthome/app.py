from datetime import datetime
import Curtain
from flask import Flask, request, jsonify
from datetime import datetime

import time

curtains = Curtain.Curtain()
app = Flask(__name__)


def log(string):
    print('my@log >', string)


def dop_plane(delay):
    next_time = time.time() + delay
    time.sleep(max(0, next_time - time.time()))
    plane_()


def plane_():
    log('delay 3000')
    if len(curtains.event) > 0:
        now_h, now_m = map(int, datetime.now().strftime('%H:%M').split(':'))
        now_time = now_h * 60 + now_m
        for i in range(len(curtains.event)):
            time_event = curtains.event[i][0] * 60 + curtains.event[i][1]
            if 0 <= now_time - time_event <= 4:
                log('EVENT_EVENT_EVENT: ' + str(i))
                curtains.state = curtains.event[i][2]
                x = curtains.event.pop(i)
                x = curtains.text.pop(i)


@app.route('/curtain/set/<value>')
def remove_curtain(value):
    curtains.chat_state = [False, None]
    try:
        if value == '1':
            curtains.state = 'open'
            log('curtains opening...')
            return 'OK'
        elif value == '0':
            curtains.state = 'close'
            log('curtains closing...')
            return 'OK'
    except:
        pass
    return 'it\'s work!'


@app.route('/curtain/state_web')
def state_web_curtain():
    log('curtain request STATE_WEB')
    if curtains.state == 'close':
        return jsonify({"status": "ok", "value": False, "text": "Штора закрыта"}), 400
    elif curtains.state == 'open':
        return jsonify({"status": "ok", "value": True, "text": "Штора открыта"}), 400
    return ''


@app.route('/curtain/state')
def state_curtain():
    log('curtain request STATE')
    plane_()
    if curtains.state == 'close':
        return '0'
    elif curtains.state == 'open':
        return '1'
    return ''


@app.route('/curtain/text/<value>/<h>/<m>')
def timer_curtain(value, h, m):
    q = 'Извините, я не понял вас. На какое время установить? (Используете: Установить на {часы} {минуты})'
    if curtains.chat_state[0]:
        if 'установить' in value and 'на' in value:
            try:
                a, b = int(h), int(m)
            except:
                return None
            print(a, b)
            if 0 <= a <= 23 and 0 <= b <= 59:
                curtains.event.append([a, b, curtains.chat_state[1]])
                curtains.text.append(str(a) + ': ' + str(b) + ' ' + curtains.chat_state[1])
                curtains.chat_state = [False, None]
                resp = 'Событие установлено!'
            else:
                resp = q
        elif ('отмен' in value and 'действ' in value) or ('не' in value and 'на какое') or ('не' in value and 'устанавл' in value):
            curtains.chat_state = [False, None]
            resp = 'Хорошо, отменяем!'
        else:
            resp = q

    elif 'установить' in value and 'время' in value and 'открыт' in value:
        resp = 'На какое время вы хотите установить открытие штор?'
        curtains.chat_state = [True, 'open']

    elif 'установить' in value and 'время' in value and 'закрыт' in value:
        resp = 'На какое время вы хотите установить закрытие штор?'
        curtains.chat_state = [True, 'close']

    elif value == 'просмотреть все события':
        return view_event()

    else:
        resp = '...'
    log(curtains.chat_state)
    log('')
    log(curtains.event)
    return jsonify({"status": "ok", "text": resp}), 400


@app.route('/curtain/view/event')
def view_event():
    curtains.chat_state = [False, None]
    resp = 'Все события,' + ','.join(curtains.text)
    return jsonify({"status": "ok", "text": resp}), 400


@app.route('/curtain/delete/event')
def delete_event():
    curtains.chat_state = [False, None]
    curtains.chat_state = []
    curtains.event = []
    return jsonify({"status": "ok", "text": 'Удаляю все события!'}), 400


# @app.teardown_request
# def teardown_request_func(error=None):
#     threading.Thread(target=lambda: dop_plane(3)).start()


if __name__ == '__main__':
    app.run(host="0.0.0.0")
