// Buffer to store incoming commands from serial port
String inData;
void setup(){
  pinMode(13, OUTPUT);  
  Serial.begin(9600);
}
void loop() {
  while (Serial.available() > 0)
  {
    char recieved = Serial.read();
    inData += recieved;

    // Process message when new line character is recieved
    if (recieved == '\n')
    {
      // You can put some if and else here to process the message juste like that:

      if (inData == "help\n") { // DON'T forget to add "\n" at the end of the string.
        Serial.println("Data_Press");
      }
      if (inData == "open\n") {
        digitalWrite(13, HIGH);
        delay(3000);
      }
      if (inData == "close\n") {
        digitalWrite(13, LOW);
        delay(3000);
      }
      if (inData == "stop\n") {
        digitalWrite(13, HIGH);
        delay(1500);
        digitalWrite(13, LOW);
        delay(1500);
      }


      inData = ""; // Clear recieved buffer
    }
  }
}
