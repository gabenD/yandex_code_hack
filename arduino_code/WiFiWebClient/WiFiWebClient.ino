// GET запрос к http://smart.locomo-net.ru


#include <ESP8266WiFi.h>

const char* ssid     = "gbphone";         // тут SSID и пароль к WIFI
const char* password = "14881488";

const char* host = "smart.locomo-net.ru";     // тут адрес сервера

bool _state_ = false;
bool _new_state_ = false;
int pin = LED_BUILTIN;
void setup() {
  pinMode(pin, OUTPUT);
  digitalWrite(pin, HIGH);
  Serial.begin(9600);
  delay(10);
  
  
  // в сетапе как обычно подключаемся к сети

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

 
  // при  включении выполняем функцию
  _state_ = op();
  sending(_state_);
}
 
void loop() {
  delay(500);
  _new_state_ = op();
  if (_new_state_ != _state_){
    _state_ = _new_state_;
    sending(_state_);
  }
}
void sending(bool data) {
  if (data) {
    Serial.print("open\n");  
    digitalWrite(pin, LOW);
  }
  else{
    Serial.print("close\n");
    digitalWrite(pin, HIGH);
  }
}
bool op() {
  String inData = Get();
  if (inData == "\n1"){
    return true;  
  }
  else{
    return false;  
  }
}
String Get() {   
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {      /// подключаемся к серверу 
    Serial.println("connection failed");
    return "";
  }
   /// если подключились, отправляем чего от сервера хотим
              // сам GET запрос с ID и ключем
    client.println("GET /curtain/state HTTP/1.1");
              // говорим к какому хосту обращаемся (на сервере может быть несколько сайтов)
    client.println("Host: smart.locomo-net.ru");
              // говорим что закончили
    client.println("Connection: close");
    client.println();
 
  delay(1000);  // ждем немного 
                // читаем ответ и отправляем его в Serial
  String line = "";
  while(client.available()){
   line = client.readStringUntil('\r');
  }
  return line;
}
