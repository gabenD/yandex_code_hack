int pins[3] = {8, 9, 13};
void setup() {
  // put your setup code here, to run once:
  for(int i = 0; i < 3; i++){
    pinMode(pins[i], OUTPUT);  
  }

}

void loop() {
  digitalWrite(8, HIGH);
  digitalWrite(9, LOW);
  digitalWrite(13, LOW);
  delay(3000);
  digitalWrite(9, HIGH);
  digitalWrite(8, LOW);
  digitalWrite(13, HIGH);
  delay(3000);

}
