#define size_p 4
int pins[] = {1, 2, 3, 4};


// the setup function runs once when you press reset or power the board
void setup() {
  for (int i = 0; i < size_p; i++){
    pinMode(pins[i], INPUT);
  }
  Serial.begin(9600);
  
}

// the loop function runs over and over again forever
void loop() {
  for (int i = 0; i < size_p; i++){
    int input = analogRead(pins[i]);
    Serial.print("A[");
    Serial.print(pins[i]);
    Serial.print("] = ");
    Serial.print(input);
    Serial.println(";");
  }
  Serial.println("----");
  delay(1000);
}
