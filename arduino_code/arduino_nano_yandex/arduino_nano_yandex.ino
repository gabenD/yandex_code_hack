#define size_g 4;
// Buffer to store incoming commands from serial port
String inData;
int left[3] = {4, 5, 3};
int right[3] = {7, 6, 8};
int pin = LED_BUILTIN;
int gerk[4] = {1, 2, 4, 3}; //Analog pins gerkons

int delay_set = 4000;
int delay_dop_set = 200;

void open_() {
  bool Flag1 = true;
  bool Flag2 = true;
  digitalWrite(left[2], HIGH);
  digitalWrite(left[1], HIGH);
  digitalWrite(left[0], LOW);

  digitalWrite(right[2], HIGH);
  digitalWrite(right[1], HIGH);
  digitalWrite(right[0], LOW);

  while (Flag1 or Flag2) {
    if (Flag1) {
      int g1 = analogRead(gerk[0]);
      if (g1 >= 900) {
        digitalWrite(left[2], LOW);
        Flag1 = false;
      }
    }
    if (Flag1) {
      int g2 = analogRead(gerk[3]);
      if (g2 >= 900) {
        digitalWrite(right[2], LOW);
        Flag2 = false;
      }
    }
  }
  m_stop();
}

void close_() {
  digitalWrite(left[2], HIGH);
  digitalWrite(left[0], HIGH);
  digitalWrite(left[1], LOW);

  digitalWrite(right[2], HIGH);
  digitalWrite(right[0], HIGH);
  digitalWrite(right[1], LOW);
  while (Flag1 or Flag2) {
    if (Flag1) {
      int g1 = analogRead(gerk[1]);
      if (g1 >= 900) {
        digitalWrite(left[2], LOW);
        Flag1 = false;
      }
    }
    if (Flag1) {
      int g2 = analogRead(gerk[2]);
      if (g2 >= 900) {
        digitalWrite(right[2], LOW);
        Flag2 = false;
      }
    }
  }
  m_stop();
}

void m_stop() {
  for (int i = 0; i < 3; i++) {
    digitalWrite(left[i], LOW);
  }
  for (int i = 0; i < 3; i++) {
    digitalWrite(right[i], LOW);
  }
}

void setup() {
  Serial.begin(9600);
  for (int i = 0; i < 3; i++) {
    pinMode(left[i], OUTPUT);
  }
  for (int i = 0; i < 3; i++) {
    pinMode(right[i], OUTPUT);
  }
  pinMode(pin, OUTPUT);
  for (int i = 0; i < size_g; i++) {
    pinMode(gerk[i], INPUT);
  }

  for (int i = 0; i < 10; i ++) {
    digitalWrite(pin, LOW);
    delay(500);
    digitalWrite(pin, HIGH);
    delay(500);
  }
}


void loop() {
  while (Serial.available() > 0)
  {
    char recieved = Serial.read();
    inData += recieved;

    // Process message when new line character is recieved
    if (recieved == '\n')
    {
      // You can put some if and else here to process the message juste like that:

      if (inData == "help\n") { // DON'T forget to add "\n" at the end of the string.
        Serial.println("Data_Press");
      }
      if (inData == "open\n") {
        open_();
        digitalWrite(pin, HIGH);
      }
      if (inData == "close\n") {
        close_();
        digitalWrite(pin, LOW);
      }
      if (inData == "stop\n") {
        m_stop();
      }


      inData = ""; // Clear recieved buffer
    }
  }
}
